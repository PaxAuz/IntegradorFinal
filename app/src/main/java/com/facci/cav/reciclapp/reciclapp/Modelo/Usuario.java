package com.facci.cav.reciclapp.reciclapp.Modelo;

public class Usuario {

    private String correo, nombre, apellido, telefono, direccion, password, verificacionPassword;

    public Usuario(){
        this.correo = correo;
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
        this.direccion = direccion;
        this.password = password;
        this.verificacionPassword = verificacionPassword;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setVerificacionPassword(String verificacionPassword) {
        this.verificacionPassword = verificacionPassword;
    }

    public String getCorreo() {

        return correo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getPassword() {
        return password;
    }

    public String getVerificacionPassword() {
        return verificacionPassword;
    }
}
